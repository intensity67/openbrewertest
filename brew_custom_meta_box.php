<?php 

//* Add Meta Box Custom Field

function breweries_add_custom_box() {  

    
    add_meta_box(
            'brewery_type_id',
            'Brewery Fields',      
            'brew_custom_box_html',  
            'breweries',
            'date_default_timezone_get()  ',
            'high',
            null                      
        );
    
}


add_action( 'add_meta_boxes', 'breweries_add_custom_box' );

function brew_custom_box_html( $post ) {

    $_brewery_id       = get_post_meta( $post->ID, '_brewery_id', true );
    $_brewery_type_id   = get_post_meta( $post->ID, '_brewery_type_id', true );
    $_street_address    = get_post_meta( $post->ID, '_street_address', true );
    // $_address1          = get_post_meta( $post->ID, '_address1', true );
    $_address2          = get_post_meta( $post->ID, '_address2', true );
    $_address3          = get_post_meta( $post->ID, '_address3', true );
    $_city              = get_post_meta( $post->ID, '_city', true );
    $_state              = get_post_meta( $post->ID, '_state', true );
    $_county_province    = get_post_meta( $post->ID, '_county_province', true );
    $_country           = get_post_meta( $post->ID, '_country', true );
    $_longitude         = get_post_meta( $post->ID, '_longitude', true );
    $_latitude          = get_post_meta( $post->ID, '_longitude', true );
    $_website_url       = get_post_meta( $post->ID, '_website_url', true );
    $_phone             = get_post_meta( $post->ID, '_phone', true );
    $_postal_code       = get_post_meta( $post->ID, '_postal_code', true );
 

    ?>

    <style scoped>

        .hcf_box{
            display: grid;
            grid-template-columns: max-content 1fr;
            grid-row-gap: 10px;
            grid-column-gap: 20px;
        }
        .hcf_field{
            display: contents;
        }

      
      </style>

    <table> 

    <tr>

       <tr><td><label for="street_address">Brewery ID: </label>
        <td> <input type="text" id="_brewery_id" name="_brewery_id" value="<?php echo $_brewery_id; ?>" disabled>

      <tr><td> <label for="_brewery_type_id">Brewery Type </label>

      <td> <select name="_brewery_type_id" id="_brewery_type_id" name="_brewery_type_id">
       
        <option value="">Select Brewery Type </option>
       
        <option value="micro" <?php if(isset($_brewery_type_id) && !empty($_brewery_type_id) && $_brewery_type_id == 'micro') echo 'selected'; ?> >micro</option>
        
        <option value="nano" <?php if(isset($_brewery_type_id) && !empty($_brewery_type_id) && $_brewery_type_id == 'nano') echo 'selected'; ?>>nano</option>
        
        <option value="regional" <?php if(isset($_brewery_type_id) && !empty($_brewery_type_id) && $_brewery_type_id == 'regional') echo 'selected'; ?>>regional</option>
       
        <option value="brewpub" <?php if(isset($_brewery_type_id) && !empty($_brewery_type_id) && $_brewery_type_id == 'brewpub') echo 'selected'; ?>>brewpub</option>
        
        <option value="large" <?php if(isset($_brewery_type_id) && !empty($_brewery_type_id) && $_brewery_type_id == 'large') echo 'selected'; ?>>large</option>
        
        <option value="planning" <?php if(isset($_brewery_type_id) && !empty($_brewery_type_id) && $_brewery_type_id == 'planning') echo 'selected'; ?>>planning</option>
       
        <option value="bar" <?php if(isset($_brewery_type_id) && !empty($_brewery_type_id) && $_brewery_type_id == 'bar') echo 'selected'; ?>>bar</option>
       
        <option value="contract" <?php if(isset($_brewery_type_id) && !empty($_brewery_type_id) && $_brewery_type_id == 'contract') echo 'selected'; ?>>contract</option>
      
        <option value="proprietor" <?php if(isset($_brewery_type_id) && !empty($_brewery_type_id) && $_brewery_type_id== 'proprietor') echo 'selected'; ?>>proprietor</option>
        
        <option value="closed" <?php if(isset($_brewery_type_id) && !empty($_brewery_type_id) && $_brewery_type_id== 'closed') echo 'selected'; ?>>closed</option>

     </select>

       <tr><td><label for="street_address">Street Address: </label>
        <td> <input type="text" id="_street_address" name="_street_address" value="<?php echo $_street_address; ?>">


       <tr><td><label for="address2">Address 2: </label>
        <td> <input type="text" id="_address2" name="_address2" value="<?php echo $_address2; ?>">

       <tr><td><label for="address3">Address 3: </label>
        <td> <input type="text" id="_address3"  name="_address3" value="<?php echo $_address3; ?>">
 
       <tr><td><label for="address3">City: </label>
        <td> <input type="text" id="_city" name="_city"  value="<?php echo $_city; ?>">

       <tr><td><label for="address3">State: </label>
        <td><input type="text" id="_state" name="_state" value="<?php echo $_state; ?>">

       <tr><td><label for="address3">County Province: </label>
        <td> <input type="text" id="_county_province"  name="_county_province"  value="<?php echo $_county_province; ?>">

       <tr><td><label for="address3">Postal Code: </label>
        <td> <input type="text" id="_postal_code" name="_postal_code" value="<?php echo $_postal_code; ?>">

       <tr><td><label for="address3">Country: </label>
        <td> <input type="text"  id="_country" name="_country" value="<?php echo $_country; ?>">


       <tr><td><label for="address3">Longitude: </label>
        <td> <input type="text" id="_longitude" name="_longitude" value="<?php echo $_longitude; ?>">

       <tr><td><label for="address3">Latitude: </label>
        <td> <input type="text" id="_latitude"  name="_latitude"value="<?php echo $_latitude; ?>">

       <tr><td><label for="address3">Phone: </label>
        <td> <input type="text" id="_phone" name="_phone" value="<?php echo $_phone; ?>">

       <tr><td><label for="address3">Website URL: </label>
        <td> <input type="text" id="_website_url" name="_website_url" value="<?php echo $_website_url; ?>">


    </table>

<?php
}

function brew_save_postdata( $post_id ) {

    $fields = [
        '_brewery_id',
        '_brewery_type_id',
        '_street_address',
        '_address2',
        '_address3',
        '_city',
        '_state',
        '_phone',
        '_postal_code',
        '_county_province',
        '_country',
        '_longitude',
        '_latitude',
        '_website_url'
     ];



    foreach ( $fields as $field ) {

        if ( array_key_exists( $field, $_POST ) ) {
            update_post_meta( $post_id, $field, $_POST[$field]);
        }
     }



}

add_action( 'save_post', 'brew_save_postdata' );