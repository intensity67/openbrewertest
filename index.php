<?php

/**

 * @package Open Brewer Test

 */

/*

Plugin Name: Open Brewer Test
Plugin URI: https://openbrewertest.intenzitia.com
Description: Open Brewer Plugin
Version: 1.0
Author: Vinz
Author URI: 
License: GPLv2 or later
Text Domain: openbrewertest

*/

include 'register_cpt.php';

include 'brew_custom_meta_box.php';

 
add_action('the_content', 'load_brews_home', 20, 1);


function load_brews_home(){
  
    global $post;

    $post_slug = $post->post_name;

 
 	//* Displays brews list on home page / home slug url

	if($post_slug == 'home'){
 
 
		$args = array(

		     'posts_per_page' => 100,
		     'post_type' => 'breweries',
		     'post_status' => 'publish'

		);

		$breweries = new WP_Query( $args );
		
		if ( $breweries->have_posts() ) {

		echo "<table>";
			
			echo "<tr>
				<td>Brewery ID
				<td>Brewery Name
				<td>Brewery Type
				<td>Street Address
				<td>Address2
				<td>Address3
				<td>City
				<td>State
				<td>Phone
				<td>Postal Code
				<td>Country Province
				<td>Country
				<td>Longitude
				<td>Latitude
				<td>Website URL
				";

		    while ( $breweries->have_posts()): $breweries->the_post();

				$custom_meta = get_post_meta(get_the_ID(), '', false);

		        echo '<tr><td>' .  $custom_meta['_brewery_id'][0]; 
				echo "<td>". 		get_the_title();
				echo "<td>".  		$custom_meta['_brewery_type_id'][0];
				echo "<td>". 		$custom_meta['_street_address'][0];
				echo "<td>".  $custom_meta['_address2'][0];
				echo "<td>".  $custom_meta['_address3'][0];
				echo "<td>".  $custom_meta['_city'][0];
				echo "<td>".  $custom_meta['_state'][0];
				echo "<td>".  $custom_meta['_phone'][0];
				echo "<td>".  $custom_meta['_postal_code'][0];
				echo "<td>".  $custom_meta['_county_province'][0];
				echo "<td>".  $custom_meta['_country'][0];
				echo "<td>".  $custom_meta['_longitude'][0];
				echo "<td>".  $custom_meta['_latitude'][0];
				echo "<td>".  $custom_meta['_website_url'][0];
  

		   endwhile;

		echo "</table>";
		
		wp_reset_postdata(); 

 		}
	
	}

}

//add_action('admin_init', 'import_brews');

register_activation_hook( __FILE__, 'import_brews' );

function import_brews(){
	
  		$curl = curl_init();

 		curl_setopt($curl, CURLOPT_URL, "https://api.openbrewerydb.org/breweries?per_page=100&page=1");

 		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

 		$output = curl_exec($curl);

		$data = (json_decode($output, true));


		foreach($data as $row){
			 

			$insert_brewery = array(
			  'post_title'    => wp_strip_all_tags( $row['name'] ),
			  'post_content'  => '',
			  'post_status'   => 'publish',
 			  'post_type'	  => 'breweries', 

			);
 
			$post_id = wp_insert_post( $insert_brewery );
 
		    $meta_fields['id'] = '_brewery_id';
		    $meta_fields['brewery_type'] = '_brewery_type_id';
		    $meta_fields['street'] = '_street_address';
		    $meta_fields['address_2'] = '_address2';
		    $meta_fields['address_3'] = '_address3';
		    $meta_fields['city'] = '_city';
		    $meta_fields['phone'] = '_phone';
		    $meta_fields['postal_code'] = '_postal_code';
		    $meta_fields['county_province'] = '_county_province';
		    $meta_fields['country'] = '_country';
		    $meta_fields['longitude'] = '_longitude';
		    $meta_fields['latitude'] = '_latitude';
		    $meta_fields['website_url'] = '_website_url';
		    $meta_fields['state'] = '_state';


		    foreach ( $meta_fields as $key => $field ) {

		        // if ( array_key_exists( $field, $data) ) {
				update_post_meta( $post_id, $field, $row[$key]);
		        // }
			}		

		}

		curl_close($curl);

}

 

 
