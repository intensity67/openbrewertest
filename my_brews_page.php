<?php
/**
Template name: My Brews

*/



?>

<div class="container-fluid">
  <div class="row">
	<table class="table table-striped">
					
		<tr>
			<td>Brewery ID
				<td>Brewery Name
						<td>Brewery Type
						<td>Street Address
						<td>Address2
						<td>City
						<td>State
						<td>Phone
						<td>Postal Code
						<td>Country Province
						<td>Country
						<td>Longitude
						<td>Latitude
						<td>Website URL
		 
			<?php  while ( $breweries->have_posts()): $breweries->the_post(); ?>

						$custom_meta = get_post_meta(get_the_ID(), '', false);

				        <?php echo '<tr><td>' .  $custom_meta['_brewery_id'][0];  ?>
						<?php echo "<td>". 		get_the_title();
						<?php echo "<td>".  		$custom_meta['_brewery_type_id'][0]; ?>
						<?php echo "<td>". 		$custom_meta['_street_address'][0]; ?>
						<?php echo "<td>".  $custom_meta['_address2'][0]; ?>
						<?php echo "<td>".  $custom_meta['_address3'][0]; ?>
						<?php echo "<td>".  $custom_meta['_city'][0]; ?>
						<?php echo "<td>".  $custom_meta['_state'][0]; ?>
						<?php echo "<td>".  $custom_meta['_phone'][0]; ?>
						<?php echo "<td>".  $custom_meta['_postal_code'][0]; ?>
						<?php echo "<td>".  $custom_meta['_county_province'][0]; ?>
						<?php echo "<td>".  $custom_meta['_country'][0]; ?>
						<?php echo "<td>".  $custom_meta['_longitude'][0]; ?>
						<?php echo "<td>".  $custom_meta['_latitude'][0]; ?>
						<?php echo "<td>".  $custom_meta['_website_url'][0]; ?>
						<?php  echo "<td>".  $custom_meta['_state'][0]; ?>
		 

				<?php endwhile; ?>

		</table>  

	</div>
</div>