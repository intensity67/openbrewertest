<?php

//* Create Post Types for Breweries

function breweries_register_post_type() {


    // create Breweries CPT

    $labels = array( 

        'name' => __( 'Breweries'  ),

        'singular_name' => __( 'Brewery'  ),

        'add_new' => __( 'Add Brewery'  ),

        'add_new_item' => __( 'Add New Brewery'  ),

        'edit_item' => __( 'Edit Brewery'  ),

        'new_item' => __( 'Add Brewery'  ),

        'view_item' => __( 'View Brewery'  ),

        'search_items' => __( 'Search Brewery'  ),

        'not_found' =>  __( 'No Brewery Found'  ),

        'not_found_in_trash' => __( 'No Brewery found in Trash'  ),

    );

    $args = array(

        'labels' => $labels,

        'has_archive' => true,

        'public' => true,

        'hierarchical' => false,

        'supports' => array(

            'title', 

            'editor', 

            'excerpt', 

            'custom-fields', 

            'thumbnail',

            'page-attributes'

        ),

        'rewrite'   => array( 'slug' => 'breweries' ),

        'show_in_rest' => true

    );

    register_post_type( 'breweries', $args );
 

}


add_action( 'init', 'breweries_register_post_type' );

